from django.db import models


class ShoeVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return f"{self.import_href}"


class Shoe(models.Model):
    fabric = models.CharField(max_length=200)
    style_name = models.CharField(max_length=200)
    bin = models.ForeignKey(ShoeVO, on_delete=models.CASCADE, related_name="bin")
