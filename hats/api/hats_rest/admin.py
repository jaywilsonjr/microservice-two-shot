from django.contrib import admin
from .models import LocationVO, Hats

admin.site.register(LocationVO)
admin.site.register(Hats)
