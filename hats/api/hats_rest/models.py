from django.db import models
from django.urls import reverse

class LocationVO(models.Model):
    import_href = models.URLField(max_length=200, null=True)

    def __str__(self):
        return self.import_href



class Hats(models.Model):
    fabric = models.CharField(max_length=100, null=True)
    style_name = models.CharField(max_length=100, null=True)
    color = models.CharField(max_length=50, null=True)
    picture_url = models.URLField(max_length=200, null=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="location",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.style_name
