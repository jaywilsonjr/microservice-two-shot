# Generated by Django 4.0.3 on 2024-05-07 14:24

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='LocationVO',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('import_href', models.URLField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Hats',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fabric', models.CharField(max_length=100, null=True)),
                ('style_name', models.CharField(max_length=100, null=True)),
                ('color', models.CharField(max_length=50, null=True)),
                ('picture_url', models.URLField(null=True)),
                ('location', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='location', to='hats_rest.locationvo')),
            ],
        ),
    ]
